package com.gannett.usat.olive.partners;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the olive_partner_ip database table.
 * 
 */
@Entity
@Table(name="olive_partner_ip")
public class OlivePartnerIp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="client_ip")
	private String clientIp;

	@Column(name="client_ip_end")
	private String clientIpEnd;

	@Column(name="date_time")
	private Timestamp dateTime;

	@Column(name="email_address")
	private String emailAddress;

	@Column(name="email_content")
	private String emailContent;

	@Column(name="email_link")
	private String emailLink;

	@Column(name="partner_id")
	private String partnerId;

	@Column(name="property_id")
	private String propertyId;

	@Column(name="property_name")
	private String propertyName;

	@Column(name="qr_link")
	private String qrLink;

    public OlivePartnerIp() {
    }

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getClientIp() {
		return this.clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getClientIpEnd() {
		return this.clientIpEnd;
	}

	public void setClientIpEnd(String clientIpEnd) {
		this.clientIpEnd = clientIpEnd;
	}

	public Timestamp getDateTime() {
		return this.dateTime;
	}

	public void setDateTime(Timestamp dateTime) {
		this.dateTime = dateTime;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmailContent() {
		return this.emailContent;
	}

	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}

	public String getEmailLink() {
		return this.emailLink;
	}

	public void setEmailLink(String emailLink) {
		this.emailLink = emailLink;
	}

	public String getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getPropertyId() {
		return this.propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	public String getPropertyName() {
		return this.propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public String getQrLink() {
		return this.qrLink;
	}

	public void setQrLink(String qrLink) {
		this.qrLink = qrLink;
	}

}