package com.gannett.usat.olive.partners;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.joda.time.DateTime;


/**
 * The persistent class for the olive_partner database table.
 * 
 */
@Entity
@Table(name="olive_partner")
public class OlivePartner implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="days_to_allow_access")
	private int daysToAllowAccess;

	@Column(name="end_date")
	private Timestamp endDate;

	@Column(name="error_page")
	private String errorPage;

	@Column(name="insert_timestamp")
	private Timestamp insertTimestamp;

	@Column(name="olive_login_url")
	private String oliveLoginUrl;

	@Column(name="olive_mobile_login_url")
	private String oliveMobileLoginUrl;

	@Column(name="olive_mobile_product_url")
	private String oliveMobileProductUrl;

	@Column(name="olive_product_restriction")
	private String oliveProductRestriction;

	@Column(name="olive_product_url")
	private String oliveProductUrl;

	@Column(name="olive_secret_key")
	private String oliveSecretKey;

	@Column(name="partner_description")
	private String partnerDescription;

	@Column(name="partner_id")
	private String partnerId;

	@Column(name="start_date")
	private Timestamp startDate;

    public OlivePartner() {
    }

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getDaysToAllowAccess() {
		return this.daysToAllowAccess;
	}

	public void setDaysToAllowAccess(int daysToAllowAccess) {
		this.daysToAllowAccess = daysToAllowAccess;
	}

	public Timestamp getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public String getErrorPage() {
		return this.errorPage;
	}

	public void setErrorPage(String errorPage) {
		this.errorPage = errorPage;
	}

	public Timestamp getInsertTimestamp() {
		return this.insertTimestamp;
	}

	public void setInsertTimestamp(Timestamp insertTimestamp) {
		this.insertTimestamp = insertTimestamp;
	}

	public String getOliveLoginUrl() {
		return this.oliveLoginUrl;
	}

	public void setOliveLoginUrl(String oliveLoginUrl) {
		this.oliveLoginUrl = oliveLoginUrl;
	}

	public String getOliveMobileLoginUrl() {
		return this.oliveMobileLoginUrl;
	}

	public void setOliveMobileLoginUrl(String oliveMobileLoginUrl) {
		this.oliveMobileLoginUrl = oliveMobileLoginUrl;
	}

	public String getOliveMobileProductUrl() {
		return this.oliveMobileProductUrl;
	}

	public void setOliveMobileProductUrl(String oliveMobileProductUrl) {
		this.oliveMobileProductUrl = oliveMobileProductUrl;
	}

	public String getOliveProductRestriction() {
		return this.oliveProductRestriction;
	}

	public void setOliveProductRestriction(String oliveProductRestriction) {
		this.oliveProductRestriction = oliveProductRestriction;
	}

	public String getOliveProductUrl() {
		return this.oliveProductUrl;
	}

	public void setOliveProductUrl(String oliveProductUrl) {
		this.oliveProductUrl = oliveProductUrl;
	}

	public String getOliveSecretKey() {
		return this.oliveSecretKey;
	}

	public void setOliveSecretKey(String oliveSecretKey) {
		this.oliveSecretKey = oliveSecretKey;
	}

	public String getPartnerDescription() {
		return this.partnerDescription;
	}

	public void setPartnerDescription(String partnerDescription) {
		this.partnerDescription = partnerDescription;
	}

	public String getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public Timestamp getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}
	public boolean isCurrentlyActive() {
		DateTime now = new DateTime();

		if (now.isAfter(this.getStartDate().getTime())) {
			if (this.getEndDate() != null) {
				if (now.isBefore(this.getEndDate().getTime())) {
					return true;
				}
			} else {
				// no end date specified
				return true;
			}
		}
		return false;
	}
}