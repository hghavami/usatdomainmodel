package com.gannett.usat.olive.partners.controller;

import com.ibm.jpa.web.JPAManager;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import com.ibm.jpa.web.NamedQueryTarget;
import com.ibm.jpa.web.Action;
import com.gannett.usat.olive.partners.OlivePartnerIp;
import java.util.List;
import javax.persistence.Query;

@SuppressWarnings("unchecked")
@JPAManager(targetEntity = com.gannett.usat.olive.partners.OlivePartnerIp.class)
public class OlivePartnerIpManager1 {

	protected static final class NamedQueries {

		protected static final String getOlivePartnerAll = "SELECT o FROM OlivePartnerIp o";}

	private EntityManagerFactory emf;

	public OlivePartnerIpManager1() {
	
	}

	public OlivePartnerIpManager1(EntityManagerFactory emf) {
		this.emf = emf;
	}

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	private EntityManager getEntityManager() {
		if (emf == null) {
			throw new RuntimeException(
					"The EntityManagerFactory is null.  This must be passed in to the constructor or set using the setEntityManagerFactory() method.");
		}
		return emf.createEntityManager();
	}

	@Action(Action.ACTION_TYPE.CREATE)
	public String createOlivePartnerIp(OlivePartnerIp olivePartnerIp) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(olivePartnerIp);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.DELETE)
	public String deleteOlivePartnerIp(OlivePartnerIp olivePartnerIp) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			olivePartnerIp = em.merge(olivePartnerIp);
			em.remove(olivePartnerIp);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.UPDATE)
	public String updateOlivePartnerIp(OlivePartnerIp olivePartnerIp) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			olivePartnerIp = em.merge(olivePartnerIp);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.FIND)
	public OlivePartnerIp findOlivePartnerIpById(long id) {
		OlivePartnerIp olivePartnerIp = null;
		EntityManager em = getEntityManager();
		try {
			olivePartnerIp = (OlivePartnerIp) em.find(OlivePartnerIp.class, id);
		} finally {
			em.close();
		}
		return olivePartnerIp;
	}

	@Action(Action.ACTION_TYPE.NEW)
	public OlivePartnerIp getNewOlivePartnerIp() {
	
		OlivePartnerIp olivePartnerIp = new OlivePartnerIp();
	
		return olivePartnerIp;
	}

	@NamedQueryTarget("getOlivePartnerAll")
	public List<OlivePartnerIp> getOlivePartnerAll() {
		EntityManager em = getEntityManager();
		List<OlivePartnerIp> results = null;
		try {
			Query query = em.createQuery(NamedQueries.getOlivePartnerAll);
			results = (List<OlivePartnerIp>) query.getResultList();
		} finally {
			em.close();
		}
		return results;
	}

}