package com.gannett.usat.olive.partners.controller;

import com.ibm.jpa.web.JPAManager;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import com.ibm.jpa.web.NamedQueryTarget;
import com.ibm.jpa.web.Action;
import com.gannett.usat.olive.partners.OlivePartnerAccessCode;
import java.util.List;
import javax.persistence.Query;

@SuppressWarnings("unchecked")
@JPAManager(targetEntity = com.gannett.usat.olive.partners.OlivePartnerAccessCode.class)
public class OlivePartnerAccessCodeManager {

	protected static final class NamedQueries {

		protected static final String getOlivePartnerAccessCode = "SELECT o FROM OlivePartnerAccessCode o WHERE o.accessCode = :accessCode";}

	private EntityManagerFactory emf;

	public OlivePartnerAccessCodeManager() {
	
	}

	public OlivePartnerAccessCodeManager(EntityManagerFactory emf) {
		this.emf = emf;
	}

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	private EntityManager getEntityManager() {
		if (emf == null) {
			throw new RuntimeException(
					"The EntityManagerFactory is null.  This must be passed in to the constructor or set using the setEntityManagerFactory() method.");
		}
		return emf.createEntityManager();
	}

	@Action(Action.ACTION_TYPE.CREATE)
	public String createOlivePartnerAccessCode(OlivePartnerAccessCode olivePartnerAccessCode) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(olivePartnerAccessCode);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.DELETE)
	public String deleteOlivePartnerAccessCode(OlivePartnerAccessCode olivePartnerAccessCode) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			olivePartnerAccessCode = em.merge(olivePartnerAccessCode);
			em.remove(olivePartnerAccessCode);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.UPDATE)
	public String updateOlivePartnerAccessCode(OlivePartnerAccessCode olivePartnerAccessCode) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			olivePartnerAccessCode = em.merge(olivePartnerAccessCode);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.FIND)
	public OlivePartnerAccessCode findOlivePartnerAccessCodeById(long id) {
		OlivePartnerAccessCode olivePartnerAccessCode = null;
		EntityManager em = getEntityManager();
		try {
			olivePartnerAccessCode = (OlivePartnerAccessCode) em.find(OlivePartnerAccessCode.class, id);
		} finally {
			em.close();
		}
		return olivePartnerAccessCode;
	}

	@Action(Action.ACTION_TYPE.NEW)
	public OlivePartnerAccessCode getNewOlivePartnerAccessCode() {
	
		OlivePartnerAccessCode olivePartnerAccessCode = new OlivePartnerAccessCode();
	
		return olivePartnerAccessCode;
	}

	@NamedQueryTarget("getOlivePartnerAccessCode")
	public List<OlivePartnerAccessCode> getAccessCode(String accessCode) {
		EntityManager em = getEntityManager();
		List<OlivePartnerAccessCode> results = null;
		try {
			Query query = em.createQuery(NamedQueries.getOlivePartnerAccessCode);
			query.setParameter("accessCode", accessCode);
			results = (List<OlivePartnerAccessCode>) query.getResultList();
		} finally {
			em.close();
		}
		return results;
	}

}