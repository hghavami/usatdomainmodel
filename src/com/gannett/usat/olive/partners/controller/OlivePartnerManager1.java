package com.gannett.usat.olive.partners.controller;

import com.ibm.jpa.web.JPAManager;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import com.ibm.jpa.web.NamedQueryTarget;
import com.ibm.jpa.web.Action;
import com.gannett.usat.olive.partners.OlivePartner;
import java.util.List;
import javax.persistence.Query;

@SuppressWarnings("unchecked")
@JPAManager(targetEntity = com.gannett.usat.olive.partners.OlivePartner.class)
public class OlivePartnerManager1 {

	protected static final class NamedQueries {

		protected static final String getAllOlivePartner = "SELECT o FROM OlivePartner o";}

	private EntityManagerFactory emf;

	public OlivePartnerManager1() {
	
	}

	public OlivePartnerManager1(EntityManagerFactory emf) {
		this.emf = emf;
	}

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	private EntityManager getEntityManager() {
		if (emf == null) {
			throw new RuntimeException(
					"The EntityManagerFactory is null.  This must be passed in to the constructor or set using the setEntityManagerFactory() method.");
		}
		return emf.createEntityManager();
	}

	@Action(Action.ACTION_TYPE.CREATE)
	public String createOlivePartner(OlivePartner olivePartner) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(olivePartner);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.DELETE)
	public String deleteOlivePartner(OlivePartner olivePartner) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			olivePartner = em.merge(olivePartner);
			em.remove(olivePartner);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.UPDATE)
	public String updateOlivePartner(OlivePartner olivePartner) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			olivePartner = em.merge(olivePartner);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.FIND)
	public OlivePartner findOlivePartnerById(long id) {
		OlivePartner olivePartner = null;
		EntityManager em = getEntityManager();
		try {
			olivePartner = (OlivePartner) em.find(OlivePartner.class, id);
		} finally {
			em.close();
		}
		return olivePartner;
	}

	@Action(Action.ACTION_TYPE.NEW)
	public OlivePartner getNewOlivePartner() {
	
		OlivePartner olivePartner = new OlivePartner();
	
		return olivePartner;
	}

	@NamedQueryTarget("getAllOlivePartner")
	public List<OlivePartner> getAllOlivePartner() {
		EntityManager em = getEntityManager();
		List<OlivePartner> results = null;
		try {
			Query query = em.createQuery(NamedQueries.getAllOlivePartner);
			results = (List<OlivePartner>) query.getResultList();
		} finally {
			em.close();
		}
		return results;
	}

}