package com.gannett.usat.olive.partners.controller;

import com.ibm.jpa.web.JPAManager;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import com.ibm.jpa.web.NamedQueryTarget;
import com.ibm.jpa.web.Action;
import com.gannett.usat.olive.partners.PartnershipUserRegistration;
import java.util.List;
import javax.persistence.Query;

@SuppressWarnings("unchecked")
@JPAManager(targetEntity = com.gannett.usat.olive.partners.PartnershipUserRegistration.class)
public class PartnershipUserRegistrationManager {

	protected static final class NamedQueries {

		protected static final String getPartnershipUserRegistrationEmail = "SELECT p FROM PartnershipUserRegistration p WHERE p.emailAddress = :emailAddress";
		protected static final String getPartnershipUserRegistrationEmailPassword = "SELECT p FROM PartnershipUserRegistration p WHERE p.emailAddress = :emailAddress AND  p.password = :password";
	}

	private EntityManagerFactory emf;

	public PartnershipUserRegistrationManager() {

	}

	public PartnershipUserRegistrationManager(EntityManagerFactory emf) {
		this.emf = emf;
	}

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	private EntityManager getEntityManager() {
		if (emf == null) {
			throw new RuntimeException(
					"The EntityManagerFactory is null.  This must be passed in to the constructor or set using the setEntityManagerFactory() method.");
		}
		return emf.createEntityManager();
	}

	@Action(Action.ACTION_TYPE.CREATE)
	public String createPartnershipUserRegistration(PartnershipUserRegistration partnershipUserRegistration) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(partnershipUserRegistration);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.DELETE)
	public String deletePartnershipUserRegistration(PartnershipUserRegistration partnershipUserRegistration) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			partnershipUserRegistration = em.merge(partnershipUserRegistration);
			em.remove(partnershipUserRegistration);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.UPDATE)
	public String updatePartnershipUserRegistration(PartnershipUserRegistration partnershipUserRegistration) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			partnershipUserRegistration = em.merge(partnershipUserRegistration);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.FIND)
	public PartnershipUserRegistration findPartnershipUserRegistrationById(long id) {
		PartnershipUserRegistration partnershipUserRegistration = null;
		EntityManager em = getEntityManager();
		try {
			partnershipUserRegistration = (PartnershipUserRegistration) em.find(PartnershipUserRegistration.class, id);
		} finally {
			em.close();
		}
		return partnershipUserRegistration;
	}

	@Action(Action.ACTION_TYPE.NEW)
	public PartnershipUserRegistration getNewPartnershipUserRegistration() {

		PartnershipUserRegistration partnershipUserRegistration = new PartnershipUserRegistration();

		return partnershipUserRegistration;
	}

	@NamedQueryTarget("getPartnershipUserRegistrationEmailPassword")
	public List<PartnershipUserRegistration> getPartnershipUserRegistrationEmailPassword(String emailAddress, String password) {
		EntityManager em = getEntityManager();
		List<PartnershipUserRegistration> results = null;
		try {
			Query query = em.createQuery(NamedQueries.getPartnershipUserRegistrationEmailPassword);
			query.setParameter("emailAddress", emailAddress);
			query.setParameter("password", password);
			results = (List<PartnershipUserRegistration>) query.getResultList();
		} finally {
			em.close();
		}
		return results;
	}

	@NamedQueryTarget("getPartnershipUserRegistrationEmail")
	public List<PartnershipUserRegistration> getPartnershipUserRegistrationEmail(String emailAddress) {
		EntityManager em = getEntityManager();
		List<PartnershipUserRegistration> results = null;
		try {
			Query query = em.createQuery(NamedQueries.getPartnershipUserRegistrationEmail);
			query.setParameter("emailAddress", emailAddress);
			results = (List<PartnershipUserRegistration>) query.getResultList();
		} finally {
			em.close();
		}
		return results;
	}

}