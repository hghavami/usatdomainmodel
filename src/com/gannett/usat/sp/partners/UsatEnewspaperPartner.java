package com.gannett.usat.sp.partners;

import java.io.Serializable;
import javax.persistence.*;

import org.joda.time.DateTime;

import java.sql.Timestamp;

/**
 * The persistent class for the usat_enewspaper_partner database table.
 * 
 */
@Entity
@Table(name = "usat_enewspaper_partner")
public class UsatEnewspaperPartner implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "ee_product_code")
	private String eeProductCode;

	@Column(name = "ee_product_restrictions")
	private String eeProductRestrictions;

	@Column(name = "end_date")
	private Timestamp endDate;

	@Column(name = "error_page_1")
	private String errorPage1;

	@Column(name = "error_page_2")
	private String errorPage2;

	@Column(name = "error_page_3")
	private String errorPage3;

	@Column(name = "partner_contact")
	private String partnerContact;

	@Column(name = "partner_description")
	private String partnerDescription;

	@Column(name = "partner_id")
	private String partnerId;

	@Column(name = "partner_name")
	private String partnerName;

	@Column(name = "secret_passphrase")
	private String secretPassphrase;

	@Column(name = "start_date")
	private Timestamp startDate;

	public UsatEnewspaperPartner() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getEeProductCode() {
		return this.eeProductCode;
	}

	public void setEeProductCode(String eeProductCode) {
		this.eeProductCode = eeProductCode;
	}

	public String getEeProductRestrictions() {
		return this.eeProductRestrictions;
	}

	public void setEeProductRestrictions(String eeProductRestrictions) {
		this.eeProductRestrictions = eeProductRestrictions;
	}

	public Timestamp getEndDate() {
		return this.endDate;
	}

	public void setEndDate(Timestamp endDate) {
		this.endDate = endDate;
	}

	public String getErrorPage1() {
		return this.errorPage1;
	}

	public void setErrorPage1(String errorPage1) {
		this.errorPage1 = errorPage1;
	}

	public String getErrorPage2() {
		return this.errorPage2;
	}

	public void setErrorPage2(String errorPage2) {
		this.errorPage2 = errorPage2;
	}

	public String getErrorPage3() {
		return this.errorPage3;
	}

	public void setErrorPage3(String errorPage3) {
		this.errorPage3 = errorPage3;
	}

	public String getPartnerContact() {
		return this.partnerContact;
	}

	public void setPartnerContact(String partnerContact) {
		this.partnerContact = partnerContact;
	}

	public String getPartnerDescription() {
		return this.partnerDescription;
	}

	public void setPartnerDescription(String partnerDescription) {
		this.partnerDescription = partnerDescription;
	}

	public String getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getPartnerName() {
		return this.partnerName;
	}

	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}

	public String getSecretPassphrase() {
		return this.secretPassphrase;
	}

	public void setSecretPassphrase(String secretPassphrase) {
		this.secretPassphrase = secretPassphrase;
	}

	public Timestamp getStartDate() {
		return this.startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public boolean isCurrentlyActive() {
		DateTime now = new DateTime();

		if (now.isAfter(this.getStartDate().getTime())) {
			if (this.getEndDate() != null) {
				if (now.isBefore(this.getEndDate().getTime())) {
					return true;
				}
			} else {
				// no end date specified
				return true;
			}

		}

		return false;
	}
}