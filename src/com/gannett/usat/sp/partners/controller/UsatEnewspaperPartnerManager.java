package com.gannett.usat.sp.partners.controller;

import com.ibm.jpa.web.JPAManager;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import com.ibm.jpa.web.NamedQueryTarget;
import com.ibm.jpa.web.Action;
import com.gannett.usat.sp.partners.UsatEnewspaperPartner;
import java.util.List;
import javax.persistence.Query;

@SuppressWarnings("unchecked")
@JPAManager(targetEntity = com.gannett.usat.sp.partners.UsatEnewspaperPartner.class)
public class UsatEnewspaperPartnerManager {

	protected static final class NamedQueries {

		protected static final String getUsatEnewspaperPartner = "SELECT u FROM UsatEnewspaperPartner u WHERE u.partnerId = :partnerId";
	}

	private EntityManagerFactory emf;

	public UsatEnewspaperPartnerManager() {

	}

	public UsatEnewspaperPartnerManager(EntityManagerFactory emf) {
		this.emf = emf;
	}

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	private EntityManager getEntityManager() {
		if (emf == null) {
			throw new RuntimeException(
					"The EntityManagerFactory is null.  This must be passed in to the constructor or set using the setEntityManagerFactory() method.");
		}
		return emf.createEntityManager();
	}

	@Action(Action.ACTION_TYPE.CREATE)
	public String createUsatEnewspaperPartner(UsatEnewspaperPartner usatEnewspaperPartner) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(usatEnewspaperPartner);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.DELETE)
	public String deleteUsatEnewspaperPartner(UsatEnewspaperPartner usatEnewspaperPartner) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			usatEnewspaperPartner = em.merge(usatEnewspaperPartner);
			em.remove(usatEnewspaperPartner);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.UPDATE)
	public String updateUsatEnewspaperPartner(UsatEnewspaperPartner usatEnewspaperPartner) throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			usatEnewspaperPartner = em.merge(usatEnewspaperPartner);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.FIND)
	public UsatEnewspaperPartner findUsatEnewspaperPartnerById(int id) {
		UsatEnewspaperPartner usatEnewspaperPartner = null;
		EntityManager em = getEntityManager();
		try {
			usatEnewspaperPartner = (UsatEnewspaperPartner) em.find(UsatEnewspaperPartner.class, id);
		} finally {
			em.close();
		}
		return usatEnewspaperPartner;
	}

	@Action(Action.ACTION_TYPE.NEW)
	public UsatEnewspaperPartner getNewUsatEnewspaperPartner() {

		UsatEnewspaperPartner usatEnewspaperPartner = new UsatEnewspaperPartner();

		return usatEnewspaperPartner;
	}

	@NamedQueryTarget("getUsatEnewspaperPartner")
	public List<UsatEnewspaperPartner> getUsatEnewspaperPartner(String partnerId) {
		EntityManager em = getEntityManager();
		List<UsatEnewspaperPartner> results = null;
		try {
			Query query = em.createQuery(NamedQueries.getUsatEnewspaperPartner);
			query.setParameter("partnerId", partnerId);
			results = (List<UsatEnewspaperPartner>) query.getResultList();
		} finally {
			em.close();
		}
		return results;
	}

}