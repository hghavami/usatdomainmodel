package com.gannett.usat.sp.partners.controller;

import com.ibm.jpa.web.JPAManager;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import com.ibm.jpa.web.Action;
import com.gannett.usat.sp.partners.UsatEnewspaperPartnerAccessLog;

@JPAManager(targetEntity = com.gannett.usat.sp.partners.UsatEnewspaperPartnerAccessLog.class)
public class UsatEnewspaperPartnerAccessLogManager {

	private EntityManagerFactory emf;

	public UsatEnewspaperPartnerAccessLogManager() {

	}

	public UsatEnewspaperPartnerAccessLogManager(EntityManagerFactory emf) {
		this.emf = emf;
	}

	public void setEntityManagerFactory(EntityManagerFactory emf) {
		this.emf = emf;
	}

	private EntityManager getEntityManager() {
		if (emf == null) {
			throw new RuntimeException(
					"The EntityManagerFactory is null.  This must be passed in to the constructor or set using the setEntityManagerFactory() method.");
		}
		return emf.createEntityManager();
	}

	@Action(Action.ACTION_TYPE.CREATE)
	public String createUsatEnewspaperPartnerAccessLog(UsatEnewspaperPartnerAccessLog usatEnewspaperPartnerAccessLog)
			throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(usatEnewspaperPartnerAccessLog);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.DELETE)
	public String deleteUsatEnewspaperPartnerAccessLog(UsatEnewspaperPartnerAccessLog usatEnewspaperPartnerAccessLog)
			throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			usatEnewspaperPartnerAccessLog = em.merge(usatEnewspaperPartnerAccessLog);
			em.remove(usatEnewspaperPartnerAccessLog);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.UPDATE)
	public String updateUsatEnewspaperPartnerAccessLog(UsatEnewspaperPartnerAccessLog usatEnewspaperPartnerAccessLog)
			throws Exception {
		EntityManager em = getEntityManager();
		try {
			em.getTransaction().begin();
			usatEnewspaperPartnerAccessLog = em.merge(usatEnewspaperPartnerAccessLog);
			em.getTransaction().commit();
		} catch (Exception ex) {
			try {
				if (em.getTransaction().isActive()) {
					em.getTransaction().rollback();
				}
			} catch (Exception e) {
				ex.printStackTrace();
				throw e;
			}
			throw ex;
		} finally {
			em.close();
		}
		return "";
	}

	@Action(Action.ACTION_TYPE.FIND)
	public UsatEnewspaperPartnerAccessLog findUsatEnewspaperPartnerAccessLogById(long id) {
		UsatEnewspaperPartnerAccessLog usatEnewspaperPartnerAccessLog = null;
		EntityManager em = getEntityManager();
		try {
			usatEnewspaperPartnerAccessLog = (UsatEnewspaperPartnerAccessLog) em.find(UsatEnewspaperPartnerAccessLog.class, id);
		} finally {
			em.close();
		}
		return usatEnewspaperPartnerAccessLog;
	}

	@Action(Action.ACTION_TYPE.NEW)
	public UsatEnewspaperPartnerAccessLog getNewUsatEnewspaperPartnerAccessLog() {

		UsatEnewspaperPartnerAccessLog usatEnewspaperPartnerAccessLog = new UsatEnewspaperPartnerAccessLog();

		return usatEnewspaperPartnerAccessLog;
	}

}