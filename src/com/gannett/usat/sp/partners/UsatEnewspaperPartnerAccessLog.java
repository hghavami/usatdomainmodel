package com.gannett.usat.sp.partners;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the usat_enewspaper_partner_access_log database table.
 * 
 */
@Entity
@Table(name="usat_enewspaper_partner_access_log")
public class UsatEnewspaperPartnerAccessLog implements Serializable {
	private static final long serialVersionUID = 1L;

	// "SA" - Success Auth; "FA" - Failed too early, "FD" - Failed post
	// departure "FI" - Failed Invalid Request, "FNA" = Failed Partner not
	// active
	public static final String SUCCESS = "SA";
	public static final String FAILED_TOO_SOON = "FA";
	public static final String FAILED_TOO_LATE = "FD";
	public static final String FAILED_INVALID = "FI";
	public static final String FAILED_NOT_ACTIVE = "FNA";

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;

	@Column(name="access_code_used")
	private String accessCodeUsed;

	@Column(name="client_ip")
	private String clientIp;

	@Column(name="client_user_agent")
	private String clientUserAgent;

	@Column(name="partner_external_key")
	private String partnerExternalKey;

	@Column(name="partner_id")
	private String partnerId;

	@Column(name="property_id")
	private String propertyId;

	@Column(name="redirected_to")
	private String redirectedTo;

	@Column(name="response_code")
	private String responseCode;

	@Column(name="time_stamp")
	private Timestamp timeStamp;

    public UsatEnewspaperPartnerAccessLog() {
    }

	public long getId() {
		return this.id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAccessCodeUsed() {
		return this.accessCodeUsed;
	}

	public void setAccessCodeUsed(String accessCodeUsed) {
		this.accessCodeUsed = accessCodeUsed;
	}

	public String getClientIp() {
		return this.clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getClientUserAgent() {
		return this.clientUserAgent;
	}

	public void setClientUserAgent(String clientUserAgent) {
		this.clientUserAgent = clientUserAgent;
	}

	public String getPartnerExternalKey() {
		return this.partnerExternalKey;
	}

	public void setPartnerExternalKey(String partnerExternalKey) {
		this.partnerExternalKey = partnerExternalKey;
	}

	public String getPartnerId() {
		return this.partnerId;
	}

	public void setPartnerId(String partnerId) {
		this.partnerId = partnerId;
	}

	public String getPropertyId() {
		return this.propertyId;
	}

	public void setPropertyId(String propertyId) {
		this.propertyId = propertyId;
	}

	public String getRedirectedTo() {
		return this.redirectedTo;
	}

	public void setRedirectedTo(String redirectedTo) {
		this.redirectedTo = redirectedTo;
	}

	public String getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public Timestamp getTimeStamp() {
		return this.timeStamp;
	}

	public void setTimeStamp(Timestamp timeStamp) {
		this.timeStamp = timeStamp;
	}

}